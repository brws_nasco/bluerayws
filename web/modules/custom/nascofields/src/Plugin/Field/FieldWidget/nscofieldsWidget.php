<?php

namespace Drupal\nscofields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'room_occupancy_widget' widget.
 *
 * @FieldWidget(
 *   id = "nascofields_normal_widget",
 *   label = @Translation("Nasco Normal widget"),
 *   field_types = {
 *     "nascofields_normal_field"
 *   }
 * )
 */
class nascofieldsNormalWidget extends WidgetBase {

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() {
        return [
            'size' => 60,
            'placeholder' => '',
                ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state) {
        $elements = [];

        $elements['size'] = [
            '#type' => 'number',
            '#title' => t('Size of textfield'),
            '#default_value' => $this->getSetting('size'),
            '#required' => TRUE,
            '#min' => 1,
        ];
        $elements['placeholder'] = [
            '#type' => 'textfield',
            '#title' => t('Placeholder'),
            '#default_value' => $this->getSetting('placeholder'),
            '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $summary = [];

        $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
        if (!empty($this->getSetting('placeholder'))) {
            $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
        }

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {


        $element['adults'] = [
            '#type' => 'textfield',
            '#title' => 'Adults',
            '#default_value' => isset($items[$delta]->adults) ? $items[$delta]->adults : NULL,
            '#size' => $this->getSetting('size'),
            '#placeholder' => $this->getSetting('placeholder'),
            '#maxlength' => $this->getFieldSetting('max_length'),
        ];

        $element['childrens'] = [
            '#type' => 'textfield',
            '#title' => 'Childrens',
            '#default_value' => isset($items[$delta]->childrens) ? $items[$delta]->childrens : NULL,
            '#size' => $this->getSetting('size'),
            '#placeholder' => $this->getSetting('placeholder'),
            '#maxlength' => $this->getFieldSetting('max_length'),
        ];
        $element['infants'] = [
            '#type' => 'textfield',
            '#title' => 'Infants',
            '#default_value' => isset($items[$delta]->infants) ? $items[$delta]->infants : NULL,
            '#size' => $this->getSetting('size'),
            '#placeholder' => $this->getSetting('placeholder'),
            '#maxlength' => $this->getFieldSetting('max_length'),
        ];

        $element['image'] = [
            '#type' => 'managed_file',
            '#title' => t('Profile Picture'),
            '#default_value' => isset($items[$delta]->image) ? array($items[$delta]->image) : NULL,
            '#upload_validators' => array(
                'file_validate_extensions' => array('gif png jpg jpeg'),
                'file_validate_size' => array(25600000),
            ),
            '#theme' => 'image_widget',
            '#preview_image_style' => 'medium',
            '#upload_location' => 'public://profile-pictures',
            '#required' => false,
        ];

//print_r($element);die;
        return $element;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        die("aaaa");
        parent::submitForm($form, $form_state);
        $akamai_file_upload = $form_state->getValue('akamai_file_upload');
        if ($form_state->getValue('akamai_file_upload') == NULL) {
            $form_state->setErrorByName('akamai_file_upload', $this->t('File.'));
        }
        else{
        // add File upload logic here
        }
    }
    
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
        if(isset($values[0]['image'][0])){
            $values[0]['image'] = $values[0]['image'][0];
        }
        /*
       // return $values;
        print_r($values);
        
//        print_r($form_state->getValues());
//        die;
         $image = $form_state->getValue('image');
        // $image = $form_state->getUserInput(); // working no fid
         print_r($image);
         die("xx");
   $file = File::load( $image[0] );
   $file->setPermanent();
   $file->save();
         * 
         */
        return $values;
    }

}
