<?php

namespace Drupal\nascofields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'nascofields_widget' widget.
 *
 * @FieldWidget(
 *   id = "nascofields_widget",
 *   label = @Translation("Room occupancy widget"),
 *   field_types = {
 *     "nascofields_field"
 *   }
 * )
 */
class nascofieldsWidget extends WidgetBase {

    /**
     * {@inheritdoc}
     */
    public static function defaultSettings() {
        return [
            'size' => 60,
            'placeholder' => '',
                ] + parent::defaultSettings();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state) {
        $elements = [];

        $elements['size'] = [
            '#type' => 'number',
            '#title' => t('Size of textfield'),
            '#default_value' => $this->getSetting('size'),
            '#required' => TRUE,
            '#min' => 1,
        ];
        $elements['placeholder'] = [
            '#type' => 'textfield',
            '#title' => t('Placeholder'),
            '#default_value' => $this->getSetting('placeholder'),
            '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsSummary() {
        $summary = [];

        $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
        if (!empty($this->getSetting('placeholder'))) {
            $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
        }

        return $summary;
    }

    /**
     * {@inheritdoc}
     */
    public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {


        $element['title'] = [
            '#type' => 'textfield',
            '#title' => 'Title',
            '#default_value' => isset($items[$delta]->title) ? $items[$delta]->title : NULL,
            '#size' => $this->getSetting('size'),
            '#placeholder' => $this->getSetting('placeholder'),
            '#maxlength' => $this->getFieldSetting('max_length'),
        ];

        $element['description'] = [
            '#type' => 'textfield',
            '#title' => 'Description',
            '#default_value' => isset($items[$delta]->description) ? $items[$delta]->description : NULL,
            '#size' => $this->getSetting('size'),
            '#placeholder' => $this->getSetting('placeholder'),
            '#maxlength' => $this->getFieldSetting('max_length'),
        ];

        $element['image'] = [
            '#type' => 'managed_file',
            '#title' => t('Profile Picture'),
            '#default_value' => isset($items[$delta]->image) ? array($items[$delta]->image) : NULL,
            '#upload_validators' => array(
                'file_validate_extensions' => array('gif png jpg jpeg'),
                'file_validate_size' => array(25600000),
            ),
            '#theme' => 'image_widget',
            '#preview_image_style' => 'medium',
            '#upload_location' => 'public://profile-pictures',
            '#required' => false,
        ];

//print_r($element);die;
        return $element;
    }

    
    
    public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
        if(isset($values[0]['image'][0])){
            $values[0]['image'] = $values[0]['image'][0];
        }
        /*
       // return $values;
        print_r($values);
        
//        print_r($form_state->getValues());
//        die;
         $image = $form_state->getValue('image');
        // $image = $form_state->getUserInput(); // working no fid
         print_r($image);
         die("xx");
   $file = File::load( $image[0] );
   $file->setPermanent();
   $file->save();
         * 
         */
        return $values;
    }

}
