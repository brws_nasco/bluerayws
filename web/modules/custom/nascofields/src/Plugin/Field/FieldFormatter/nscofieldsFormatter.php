<?php

namespace Drupal\nscofields\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'room_occupancy_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "nascofields_normal_formatter",
 *   label = @Translation("Nasco Normal Field"),
 *   field_types = {
 *     "nascofields_normal_field"
 *   }
 * )
 */
class nascofieldsNormalFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
        'aaa'
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = ['#markup' => $this->viewValue($item)."xxxx"];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(\Drupal\room_occupancy\Plugin\Field\FieldType\roomOccupancyField $item) {
      
      $output = [];
      foreach($item->getProperties() as $name=>$pro){
          /* @var $pro Drupal\Core\TypedData\Plugin\DataType\StringData */
          $output []= sprintf('%s: %s',$name,$item->get($name)->getValue());
          //echo get_class($pro);
          // die;
          
          
         // $output .= sprintf('%s: %s',$pro->getLabel(),$pro->value);
      }
      
      if($item->get('image')){
            $file = \Drupal\file\Entity\File::load($item->get('image')->getValue());
            //$path = $file->url();
            $path = $file->getFileUri();
            $style = \Drupal::entityTypeManager()->getStorage('image_style')->load('thumbnail');
            $imagestyle = $style->buildUrl($path);
            $output['imageurl'] = $path;
            $output['imagestyle'] = '<img src="'.$imagestyle.'"/>';
      }
              
      return implode(', ', $output);
      echo $output;
      die;
      return $output;
       print_r($item->get('value')->getValue());
       print_r($item->get('adults')->getValue());
       print_r($item->get('childrens')->getValue());
      die;
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
