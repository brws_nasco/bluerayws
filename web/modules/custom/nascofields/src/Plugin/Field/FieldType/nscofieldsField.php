<?php

namespace Drupal\nscofields\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'nascofields_normal_field' field type.
 *
 * @FieldType(
 *   id = "nascofields_normal_field",
 *   label = @Translation("Nasco Normal Field"),
 *   description = @Translation("Nasco Normal Field"),
 *   default_widget = "nascofields_normal_widget",
 *   default_formatter = "nascofields_normal_formatter"
 * )
 */
class nascofieldsNormalField extends FieldItemBase {

    /**
     * {@inheritdoc}
     */
    public static function defaultStorageSettings() {
        return [
            'max_length' => 255,
            'is_ascii' => FALSE,
            'case_sensitive' => FALSE,
                ] + parent::defaultStorageSettings();
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        // Prevent early t() calls by using the TranslatableMarkup.
        $properties = [];
        /*
          $properties['value'] = DataDefinition::create('string')
          ->setLabel(new TranslatableMarkup('Text value'))
          ->setSetting('case_sensitive', $field_definition->getSetting('case_sensitive'))
          ->setRequired(TRUE);
         */
        $properties['adults'] = DataDefinition::create('string')->setLabel(t('Adults'));
        $properties['childrens'] = DataDefinition::create('string')->setLabel(t('Childrens'));
        $properties['infants'] = DataDefinition::create('string')->setLabel(t('Infants'));
        $properties['image'] = DataDefinition::create('integer')->setLabel(t('Infants'));
        
        /*
        $properties['image'] = BaseFieldDefinition::create('image')
                ->setLabel(t('Image'))
                ->setDescription(t('The product image.'))
                ->setDisplayOptions('form', array(
            'type' => 'image_image',
            'weight' => 5,
        ));
        */

        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        $schema = [
            'columns' => [
                /*
                  'value' => [
                  'type' => $field_definition->getSetting('is_ascii') === TRUE ? 'varchar_ascii' : 'varchar',
                  'length' => (int) $field_definition->getSetting('max_length'),
                  'binary' => $field_definition->getSetting('case_sensitive'),
                  ],
                 */
                'adults' => ['type' => 'int', 'size' => 'tiny', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0],
                'childrens' => ['type' => 'int', 'size' => 'tiny', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0],
                'infants' => ['type' => 'int', 'size' => 'tiny', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0],
                'image' => ['type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => NULL],
            ],
        ];

        return $schema;
    }

    /**
     * {@inheritdoc}
     */
    /*
      public function getConstraints() {
      $constraints = parent::getConstraints();

      if ($max_length = $this->getSetting('max_length')) {
      $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
      $constraints[] = $constraint_manager->create('ComplexData', [
      'value' => [
      'Length' => [
      'max' => $max_length,
      'maxMessage' => t('%name: may not be longer than @max characters.', [
      '%name' => $this->getFieldDefinition()->getLabel(),
      '@max' => $max_length
      ]),
      ],
      ],
      ]);
      }

      return $constraints;
      }
     */

    /**
     * {@inheritdoc}
     */
    public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
        $random = new Random();
        $values['value'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
        return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {

        $elements = [];

        $elements['max_length'] = [
            '#type' => 'number',
            '#title' => t('Maximum length'),
            '#default_value' => $this->getSetting('max_length'),
            '#required' => TRUE,
            '#description' => t('The maximum length of the field in characters.'),
            '#min' => 1,
            '#disabled' => $has_data,
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {

        $value = $this->get('adults')->getValue();

        return $value === NULL || $value === '';
    }

    public function getAdults() {
        $def = $this->getProperties();
        if (key_exists('adults', $def)) {
            return $this->get('adults')->getValue();
        }
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function setValue($values, $notify = TRUE) {
        if (isset($values['adults']) && is_array($values)) {
            // Normal textarea's and textfields put their values simply in by
            // array($name => $value); Unfortunately text_format textareas put
            // them into an array so also the format gets saved: array($name
            // => array('value' => $value, 'format' => $format)).
            // So the API will try to save normal textfields to the 'name' field
            // and text_format fields to 'answer_value' and 'answer_format'.
            // To bypass this, we pull the values out of this array and force
            // them to be saved in 'answer' and 'answer_format'.
            $values['adults'] = $values['adults'];
            $values['childrens'] = $values['childrens'];
            $values['infants'] = $values['infants'];
            $values['image'] = $values['image'];
        }
        parent::setValue($values, $notify);
    }

}
