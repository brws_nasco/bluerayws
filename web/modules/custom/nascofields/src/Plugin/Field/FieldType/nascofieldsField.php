<?php

namespace Drupal\nascofields\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'nascofields_field' field type.
 *
 * @FieldType(
 *   id = "nascofields_field",
 *   label = @Translation("Nasco Field"),
 *   description = @Translation("Nasco Field"),
 *   default_widget = "nascofields_widget",
 *   default_formatter = "nascofields_formatter"
 * )
 */
class nascofieldsField extends FieldItemBase {

    /**
     * {@inheritdoc}
     */
    public static function defaultStorageSettings() {
        return [
            'max_length' => 255,
            'is_ascii' => FALSE,
            'case_sensitive' => FALSE,
                ] + parent::defaultStorageSettings();
    }

    /**
     * {@inheritdoc}
     */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
        // Prevent early t() calls by using the TranslatableMarkup.
        $properties = [];
        $properties['title'] = DataDefinition::create('string')->setLabel(t('Adults'));
        $properties['description'] = DataDefinition::create('string')->setLabel(t('Childrens'));
        $properties['image'] = DataDefinition::create('integer')->setLabel(t('Infants'));
        return $properties;
    }

    /**
     * {@inheritdoc}
     */
    public static function schema(FieldStorageDefinitionInterface $field_definition) {
        $schema = [
            'columns' => [
                'title' => ['type' => 'varchar', 'length' => 255],
                'description' => ['type' => 'text', 'size' => 'big',],
                'image' => ['type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => NULL],
            ],
        ];

        return $schema;
    }

   
    public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
        $random = new Random();
        $values['value'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
        return $values;
    }

    /**
     * {@inheritdoc}
     */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {

        $elements = [];

        $elements['max_length'] = [
            '#type' => 'number',
            '#title' => t('Maximum length'),
            '#default_value' => $this->getSetting('max_length'),
            '#required' => TRUE,
            '#description' => t('The maximum length of the field in characters.'),
            '#min' => 1,
            '#disabled' => $has_data,
        ];

        return $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function isEmpty() {

        $value = $this->get('title')->getValue();

        return $value === NULL || $value === '';
    }

    public function getAdults() {
        $def = $this->getProperties();
        if (key_exists('title', $def)) {
            return $this->get('title')->getValue();
        }
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function setValue($values, $notify = TRUE) {
        if (isset($values['title']) && is_array($values)) {
            // Normal textarea's and textfields put their values simply in by
            // array($name => $value); Unfortunately text_format textareas put
            // them into an array so also the format gets saved: array($name
            // => array('value' => $value, 'format' => $format)).
            // So the API will try to save normal textfields to the 'name' field
            // and text_format fields to 'answer_value' and 'answer_format'.
            // To bypass this, we pull the values out of this array and force
            // them to be saved in 'answer' and 'answer_format'.
            $values['title'] = $values['title'];
            $values['description'] = $values['description'];
            $values['image'] = $values['image'];
        }
        parent::setValue($values, $notify);
    }

}
